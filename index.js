const data_api = "http://159.203.64.136:8081";
const upload_api = "http://159.203.64.136:8080";

let store = {
    algorithms: [
        'cosine', 'kmeans', 'knn', 'csr', 'matrix-decomposition'
    ],
    records: [],
    uploads: []
};

let upload = () => {

    var input = document.querySelector('input[type="file"]');

    var data = new FormData();
    data.append('file', input.files[0]);
    data.append('algorithm', document.getElementById("algorithm").value);
    data.append('k', document.getElementById("k").value);




    const options = {
        method: 'POST',
        body: data,
        mode: 'cors'
    };
    fetch(upload_api + '/uploads/' + store.login._id, options).then((res) => {
        getData(store.login._id);
    });
};

let retrain = (id) => {

    const options = {
        method: 'POST',
        body: JSON.stringify({
            algorithm: document.getElementById("algorithm").value,
            k: document.getElementById("k").value
        }),
        headers: new Headers({
            'Content-Type': "application/json; charset=utf-8",
            'Accept': 'application/json'
        }),
        mode: 'cors'
    };
    fetch(upload_api + '/uploads/train/' + id, options).then((res) => {
        getData(store.login._id);
    });
};

let getData = (userId) => {
    fetch(upload_api + '/uploads/' + userId, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': "application/json; charset=utf-8",
            'Accept': 'application/json'
        }),
        mode: 'cors'
    }).then((res) => {
        res.json().then((body) => {
            store.uploads = body.uploads;
        });
    });
    fetch(data_api + '/records/' + userId, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': "application/json; charset=utf-8",
            'Accept': 'application/json'
        }),
        mode: 'cors'
    }).then((res) => {
        res.json().then((body) => {
            store.records = body.records;
            document.getElementById("app").innerHTML = dashboard();
        });
    });
}

let getDataCard = (record) => {
    return `
    <div class="card" style="width: 20rem;">
    <div class="card-block">
      <h4 class="card-title">${record.algorithm}</h4>
      <p class="card-text">userId: ${record.userId}</p>
      <p class="card-text">_id: ${record._id}</p>
      <a href="${record.url}" class="card-text">Url: ${record.url}</a>
      <ul>
        ${record.centroids ? record.centroids.map((centroid) => {
            return "<li>"+centroid+"</li>";
        }) : ""}
      </ul>
      ${record.matrixVUrl ? '<a href="'+record.matrixVUrl+'" class="card-text">matrixVUrl: '+record.matrixVUrl+'</a>' : ""}

      <button class="btn btn-primary" onclick="query()">Query</a>
    </div>
  </div>
    `;
};

let getUploadCard = (upload) => {
    return `
    <div class="card" style="width: 20rem;">
    <div class="card-block">
      <h4 class="card-title">${upload.algorithm}</h4>
      <p class="card-text">_id: ${upload._id}</p>
      <p class="card-text">userId: ${upload.userId}</p>
      <p class="card-text">uploadId: ${upload.uploadId}</p>
      <a href="${upload.url}" class="card-text">Url: ${upload.fileName}</a>

      <button class="btn btn-primary" onclick="retrain('${upload._id}')">Re-Train</a>
    </div>
  </div>
    `;
};

let dashboard = () => {
    return `
    <div class="container">
        <div class="row">
            <div class="container">
                <div class="row">
                    <h1>New Upload</h1>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="">CSV File to Upload</label>
                            <input type="file" class="form-control-file" name="" id="file" placeholder="" aria-describedby="fileHelpId">
                            <small id="fileHelpId" class="form-text text-muted">Help text</small>
                            
                            <label for="exampleSelect1">Algorithm</label>
                            <select class="form-control" id="algorithm">
                                ${store.algorithms.map((algo) => {
                                    return "<option>"+algo+"</option>";
                                })}
                            </select>
                            <label>K</label>
                            <input type="number" name="" id="k" class="form-control" placeholder="3" aria-describedby="helpId">
                            <small id="helpId" class="text-muted">this value is only used for kmeans</small>
                        </div>
                    </div>
                    <div class="col">
                        <button class="btn btn-primary" onclick="upload()">Upload</a>
                    </div>
                </div>
            </div>

        </div>
        <hr/>
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <h2>Data:</h2>
                    ${store.records.map((record) => {
                        return getDataCard(record);
                    })}
                </div>
                <div class="col-6">
                    <h2>Uploads:</h2>
                    ${store.uploads.map((upload) => {
                        return getUploadCard(upload);
                    })}
                </div>
            </div>
        </div>
    </div>
`;
};

let login = () => {
    const options = {
        method: 'POST',
        body: JSON.stringify({
            email: document.getElementById("email").value,
            password: document.getElementById("password").value
        }),
        headers: new Headers({
            'Content-Type': "application/json; charset=utf-8",
            'Accept': 'application/json'
        }),
        mode: 'cors'
    };
    fetch(upload_api + '/users/login', options).then((res) => {
        res.json().then((body) => {
            store.login = body;
            getData(body._id);
        });
    }).catch((err) => {
        console.log(err);
    });
};

let signUp = () => {
    const options = {
        method: 'POST',
        body: JSON.stringify({
            email: document.getElementById("email").value,
            password: document.getElementById("password").value
        }),
        headers: new Headers({
            'Content-Type': "application/json; charset=utf-8",
            'Accept': 'application/json'
        }),
        mode: 'cors'
    };
    fetch(upload_api + '/users/signup', options).then((res) => {
        res.json().then((body) => {
            store.login = body;
            getData(body._id);
        })
    }).catch((err) => {
        console.log(err);
    });
}